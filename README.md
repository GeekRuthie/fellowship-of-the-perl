# The Fellowship of the Perl

## Running locally

1. Get a copy of this repo on your development machine
1. [Install Jekyll](https://jekyllrb.com/docs/installation/) (if not
   already installed)
1. Run `bundle config set path 'vendor/bundle'`
1. Run `bundle update` to install deps
1. Run `jekyll serve` to serve a local copy of the site

Note that you may need to edit `_config.yml` to remove the dedicated
host IP.
